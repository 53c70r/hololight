import requests
import random
import math
import time
import numpy
import sys
import argparse
from scipy import stats

URL = "http://hololicht.s0/holodeck/all/"


class Range(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def __eq__(self, other):
        return self.start <= other <= self.end


def call_url(r, g, b, w):
    r = str('{:.8f}'.format(round(r, 8)))
    g = str('{:.8f}'.format(round(g, 8)))
    b = str('{:.8f}'.format(round(b, 8)))
    w = str('{:.8f}'.format(round(w, 8)))
    req = requests.get(URL + r + "," + g + "," + b + "," + w)
    if args.verbose:
        print("R,G,B,W: " + r + "," + g + "," + b + "," + w)
        print("status code: " + str(req.status_code))


def generate_random_color(w, brightness):
    r = random.uniform(0, brightness)
    g = random.uniform(0, brightness)
    b = random.uniform(0, brightness)
    return r, g, b, w


def generate_rainbow(x, w, wght, brightness):
    r = math.sin(x)
    g = math.sin(x + math.radians(120))
    b = math.sin(x + math.radians(240))
    r = brightness * stats.norm.cdf(r, 0, wght)
    g = brightness * stats.norm.cdf(g, 0, wght)
    b = brightness * stats.norm.cdf(b, 0, wght)
    return r, g, b, w


def generate_single_color(r, g, b, w):
    return r, g, b, w


parser = argparse.ArgumentParser(description="control hololicht.s0")
parser.add_argument("mode", choices=["rainbow", "random", "color", "on", "off"], help="set the mode of operation")
parser.add_argument("-v", "--verbose", action="store_true", help="set verbose")
parser.add_argument("--interval", help="refresh interval in seconds", type=float, choices=[Range(0.001, 10.0)], metavar="[0.10-10.0]", default=0.1)
parser.add_argument("--steps", help="steps in float", type=float, choices=[Range(0.001, 1.0)], metavar="[0.01-1.0]", default=0.005)
parser.add_argument("--weighting", help="weighting for rainbow", type=float, choices=[Range(0.0, 1.0)], metavar="[0.0-1.0]", default=0.1)
parser.add_argument("--brightness", help="maximum brightness of all color channels", type=float, choices=[Range(0.0, 1.0)], metavar="[0.0-1.0]", default=1.0)
parser.add_argument("--white", help="brightness of white channel", type=float, choices=[Range(0.0, 1.0)], metavar="[0.0-1.0]", default=0)
parser.add_argument("--red", help="brightness of red channel", type=float, choices=[Range(0.0, 1.0)], metavar="[0.0-1.0]", default=0)
parser.add_argument("--green", help="brightness of green channel", type=float, choices=[Range(0.0, 1.0)], metavar="[0.0-1.0]", default=0)
parser.add_argument("--blue", help="brightness of blue channel", type=float, choices=[Range(0.0, 1.0)], metavar="[0.0-1.0]", default=0)

args = parser.parse_args()
white = args.white
weighting = args.weighting
steps = args.steps
interval = args.interval
brightness = args.brightness
red = args.red
green = args.green
blue = args.blue

if args.mode == "rainbow":
    x = random.randint(0, 100)
    while(1):
        a = generate_rainbow(x, white, weighting, brightness)
        call_url(*a)
        x = x + (steps)
        time.sleep(interval)

if args.mode == "random":
    call_url(*generate_random_color(args.white, args.brightness))

if args.mode == "on":
    call_url(brightness, brightness, brightness, brightness)

if args.mode == "off":
    req = requests.get(URL + "night")
    if args.verbose:
        print("status code: " + str(req.status_code))

if args.mode == "color":
    call_url(*generate_single_color(red, green, blue, white))
